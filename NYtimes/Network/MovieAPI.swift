//
//  MovieAPI.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//

import Foundation
import Alamofire

class MovieAPI {
    typealias CompletionBlock = (_ success: Data) -> Void
    typealias ErrorBlock = (_ error: Any) -> Void
    
    private static let apiKey = "api-key"
    private static let apiKeyValue  = "99dba655b7e54a89a90b8dfd613b2ac3"
    
    private static let baseURLStr = "https://api.nytimes.com/svc/movies/v2/"
    private static let reviewsURLStr = "reviews/search.json"
    private static let criticsURLStr = "critics/all.json"
    
    static func request(for urlStr: String, paramsDict: [String: Any]) -> DataRequest? {
        guard let reviewURL = URL(string: baseURLStr + urlStr) else {
            return nil
        }
        var parameters: Parameters = [
            apiKey: apiKeyValue,
        ]
        parameters.merge(paramsDict) { (value1, value2) -> Any in
            return value1
        }
        
        return Alamofire.request(reviewURL, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString), headers: nil)
    }
    
    static func reviews( offset: Int = 0, stringToSearch: String = "", dateStringToSearch: String = "", succes: @escaping CompletionBlock, failure: @escaping ErrorBlock) {
        if let request = self.request(for: self.reviewsURLStr, paramsDict: ["offset": String(offset),
                                                                       "query": stringToSearch,
                                                                       "publication-date": dateStringToSearch]) {
            request.response(completionHandler: { (response) in
                if let realData = response.data {
                    print("success recieve reviews/search")
                    succes(realData)
                } else {
                    failure("No data")
                }
            })
        } else {
            failure("Request create unsuccessfull")
        }
    }
    
    static func critics( succes: @escaping CompletionBlock, failure: @escaping ErrorBlock) {
        if let request = self.request(for: self.criticsURLStr, paramsDict: [:]) {
            request.response(completionHandler: { (response) in
                if let realData = response.data {
                    print("success recieve critics")
                    succes(realData)
                } else {
                    failure("No data")
                }
            })
        } else {
            failure("Request create unsuccessfull")
        }
    }
    
}
