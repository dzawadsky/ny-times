//
//  Extensions.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 23.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//

import UIKit

class Extensions: NSObject {

}

extension String {
    var htmlDecoded: String {
        let decoded = try? NSAttributedString(data: Data(utf8), options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
            ], documentAttributes: nil).string
        
        return decoded ?? self
    }
}
