//
//  Connector.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//

import UIKit
import CoreData

class Connector: NSObject {

    static var appDelegate: AppDelegate = UIApplication.shared.delegate! as! AppDelegate
    static var viewContext: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
    static var progressIndicatorIsVisible = UIApplication.shared.isNetworkActivityIndicatorVisible
    
    static var moviesFetchedResultsController = appDelegate.moviesFetchedResultsController
    static var criticsFetchedResultsController = appDelegate.criticsFetchedResultsController
    static var moviesHolder = MoviesHolder.sharedInstance
    static var criticsHolder = CriticHolder.sharedInstance
    
    static var searchMode: Bool {
        get {
            return MoviesHolder.sharedInstance.searchMode
        }
        set {
            MoviesHolder.sharedInstance.searchMode = newValue
        }
    }
    
    static var dateMode: Bool {
        get {
            return MoviesHolder.sharedInstance.dateMode
        }
        set {
            if newValue == false {
                MoviesHolder.sharedInstance.dateFilterString = ""
            }
            MoviesHolder.sharedInstance.dateMode = newValue
        }
    }
    static var dateFilterString: String {
        get {
            return MoviesHolder.sharedInstance.dateFilterString
        }
        set {
            MoviesHolder.sharedInstance.dateFilterString = newValue
        }
    }
    
    static var moviesCount: Int {
        get {
            do {
                try moviesFetchedResultsController.performFetch()
                if let objects = moviesFetchedResultsController.fetchedObjects {
                    return objects.count
                } else {
                    return 0
                }
            } catch {
                print(error)
                return -1
            }
        }
    }
    
    static var criticsCount: Int {
        get {
            do {
                try criticsFetchedResultsController.performFetch()
                if let objects = criticsFetchedResultsController.fetchedObjects {
                    return objects.count
                } else {
                    return 0
                }
            } catch {
                print(error)
                return -1
            }
        }
    }
}
