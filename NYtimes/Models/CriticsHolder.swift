//
//  CriticModel.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//

import UIKit

class CriticHolder: NSObject, Codable {

    private static var numOfCritics = 0
    
    var status = ""
    var copyright = ""
    var numResults = 0
    var results = [Critic]()
    
    var count: Int {
        get {
            return results.count
        }
    }
    
    static let sharedInstance = CriticHolder()
    
    //Modes
    var searchMode = false
    
    var dateMode = false
    var dateFilterString = ""
    
    enum CodingKeys: String, CodingKey {
        case status
        case copyright
        case numResults = "num_results"
        case results
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.init()
        
        self.status = try container.decodeIfPresent(String.self, forKey: .status) ?? ""
        self.copyright = try container.decodeIfPresent(String.self, forKey: .copyright) ?? ""
        self.numResults = try container.decodeIfPresent(Int.self, forKey: .numResults) ?? 0
        self.results = try container.decodeIfPresent([Critic].self, forKey: .results) ?? [Critic]()
        
        CriticHolder.sharedInstance.status = self.status
        CriticHolder.sharedInstance.copyright = self.copyright
        CriticHolder.sharedInstance.numResults = self.numResults
        CriticHolder.sharedInstance.results = self.results
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(CriticHolder.sharedInstance.status, forKey: .status)
        try container.encode(CriticHolder.sharedInstance.copyright, forKey: .copyright)
        try container.encode(CriticHolder.sharedInstance.numResults, forKey: .numResults)
        try container.encode(CriticHolder.sharedInstance.results, forKey: .results)
    }
    
    static private func resetActiveHolder() {
        sharedInstance.status = ""
        sharedInstance.copyright = ""
        sharedInstance.numResults = 0
        sharedInstance.results.removeAll()
    }
    
    func parseResponse( responseData: Data, completionHandler: (CriticHolder?) -> Void) {
        let decoder = JSONDecoder()
        do {
            let criticsResponse = try decoder.decode(CriticHolder.self, from: responseData)
            completionHandler(criticsResponse)
            return
        } catch {
            print(error)
        }
        completionHandler(nil)
    }
    
    static func nextIndex() -> Int {
        numOfCritics = numOfCritics + 1
        return numOfCritics
    }
    
}
