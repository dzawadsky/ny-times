//
//  MultimediaCritic+CoreDataProperties.swift
//  
//
//  Created by Dmitry Zawadsky on 25.01.2018.
//
//

import Foundation
import CoreData


extension MultimediaCritic {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MultimediaCritic> {
        return NSFetchRequest<MultimediaCritic>(entityName: "MultimediaCritic")
    }

    @NSManaged public var resource: Resource

}

// MARK: Generated accessors for resource
extension MultimediaCritic {

    @objc(addResourceObject:)
    @NSManaged public func addToResource(_ value: Resource)

    @objc(removeResourceObject:)
    @NSManaged public func removeFromResource(_ value: Resource)

    @objc(addResource:)
    @NSManaged public func addToResource(_ values: NSSet)

    @objc(removeResource:)
    @NSManaged public func removeFromResource(_ values: NSSet)

}
