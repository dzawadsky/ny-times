//
//  Resource+CoreDataClass.swift
//  
//
//  Created by Dmitry Zawadsky on 24.01.2018.
//
//

import Foundation
import CoreData

@objc(Resource)
public class Resource: NSManagedObject, Codable {

    static let entityName = "Resource"
    
    enum CodingKeys: String, CodingKey {
        case type
        case src
        case height
        case width
        case credit
    }
    
    public convenience init() {
        let context = Connector.viewContext
        guard let resource = NSEntityDescription.entity(forEntityName: Resource.entityName, in: context) else { fatalError() }
        
        self.init(entity: resource, insertInto: context)
        
        self.type = ""
        self.src = ""
        self.height = NSNumber(value: -1)
        self.width = NSNumber(value: -1)
        self.credit = ""
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let context = Connector.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: Resource.entityName, in: context) else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.type = try container.decodeIfPresent(String.self, forKey: .type) ?? ""
        self.src = try container.decodeIfPresent(String.self, forKey: .src) ?? ""
        self.width = NSNumber(value: (try container.decodeIfPresent(Int64.self, forKey: .width) ?? 0))
        self.height = NSNumber(value: (try container.decodeIfPresent(Int64.self, forKey: .height) ?? 0))
        self.credit = try container.decodeIfPresent(String.self, forKey: .credit) ?? ""
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.type, forKey: .type)
        try container.encode(self.src, forKey: .src)
        try container.encode(self.width?.int64Value, forKey: .width)
        try container.encode(self.height?.int64Value, forKey: .height)
        try container.encode(self.credit, forKey: .credit)
    }
    
}
