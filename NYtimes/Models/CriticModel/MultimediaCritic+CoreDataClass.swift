//
//  MultimediaCritic+CoreDataClass.swift
//  
//
//  Created by Dmitry Zawadsky on 25.01.2018.
//
//

import Foundation
import CoreData

@objc(MultimediaCritic)
public class MultimediaCritic: NSManagedObject, Codable {
    static let entityName = "MultimediaCritic"
    
    enum CodingKeys: String, CodingKey {
        case resource
    }
    
    public convenience init() {
        let context = Connector.viewContext
        guard let multimedia = NSEntityDescription.entity(forEntityName: MultimediaCritic.entityName, in: context) else { fatalError() }
        
        self.init(entity: multimedia, insertInto: context)
        
        self.resource = Resource()
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let context = Connector.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: MultimediaCritic.entityName, in: context) else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let tmp = try container.decodeIfPresent(Dictionary<String, AnyJSONType>.self, forKey: .resource) {
            self.resource = Resource()
            if let credit = tmp["credit"]?.jsonValue as? String {
                self.resource.credit = credit
            }
            
            if let src = tmp["src"]?.jsonValue as? String {
                self.resource.src = src
            }
            
            if let height = tmp["height"]?.jsonValue as? Int {
                self.resource.height = NSNumber(value: height)
            }
            
            if let width = tmp["width"]?.jsonValue as? Int {
                self.resource.width = NSNumber(value: width)
            }

            if let type = tmp["type"]?.jsonValue as? String {
                self.resource.type = type
            }
        }
        //self.resource = try container.decodeIfPresent(Dictionary.self, forKey: .resource)!
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.resource, forKey: .resource)
    }
}

public protocol JSONType: Decodable {
    var jsonValue: Any { get }
}

extension Int: JSONType {
    public var jsonValue: Any { return self }
}
extension String: JSONType {
    public var jsonValue: Any { return self }
}
extension Double: JSONType {
    public var jsonValue: Any { return self }
}
extension Bool: JSONType {
    public var jsonValue: Any { return self }
}

public struct AnyJSONType: JSONType {
    public let jsonValue: Any
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        if let intValue = try? container.decode(Int.self) {
            jsonValue = intValue
        } else if let stringValue = try? container.decode(String.self) {
            jsonValue = stringValue
        } else if let boolValue = try? container.decode(Bool.self) {
            jsonValue = boolValue
        } else if let doubleValue = try? container.decode(Double.self) {
            jsonValue = doubleValue
        } else if let doubleValue = try? container.decode(Array<AnyJSONType>.self) {
            jsonValue = doubleValue
        } else if let doubleValue = try? container.decode(Dictionary<String, AnyJSONType>.self) {
            jsonValue = doubleValue
        } else {
            throw DecodingError.typeMismatch(JSONType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Unsupported JSON tyep"))
        }
    }
}
