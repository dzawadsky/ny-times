//
//  Resource+CoreDataProperties.swift
//  
//
//  Created by Dmitry Zawadsky on 24.01.2018.
//
//

import Foundation
import CoreData


extension Resource {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Resource> {
        return NSFetchRequest<Resource>(entityName: "Resource")
    }

    @NSManaged public var type: String?
    @NSManaged public var src: String?
    @NSManaged public var height: NSNumber?
    @NSManaged public var width: NSNumber?
    @NSManaged public var credit: String?

}
