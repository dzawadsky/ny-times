//
//  Critic+CoreDataClass.swift
//  
//
//  Created by Dmitry Zawadsky on 24.01.2018.
//
//

import Foundation
import CoreData

@objc(Critic)
public class Critic: NSManagedObject, Codable {
    
    static let criticCacheName = "criticCache"
    static let entityName = "Critic"

    enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
        case sortName = "sort_name"
        case status
        case bio
        case seoName = "seo_name"
        case multimedia
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let context = Connector.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: Critic.entityName, in: context) else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.index = NSNumber(value: CriticHolder.nextIndex());
        self.displayName = try container.decodeIfPresent(String.self, forKey: .displayName) ?? ""
        self.sortName = try container.decodeIfPresent(String.self, forKey: .sortName) ?? ""
        self.status = (try container.decodeIfPresent(String.self, forKey: .status) ?? "").htmlDecoded
        self.bio = (try container.decodeIfPresent(String.self, forKey: .bio) ?? "").htmlDecoded
        self.seoName = try container.decodeIfPresent(String.self, forKey: .seoName) ?? ""
        self.multimedia = try container.decodeIfPresent(MultimediaCritic?.self, forKey: .multimedia) ?? MultimediaCritic()
                
        Connector.appDelegate.saveContext()
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.displayName, forKey: .displayName)
        try container.encode(self.sortName, forKey: .sortName)
        try container.encode(self.status, forKey: .status)
        try container.encode(self.bio, forKey: .bio)
        try container.encode(self.seoName, forKey: .seoName)
        try container.encode(self.multimedia, forKey: .multimedia)
    }
}
