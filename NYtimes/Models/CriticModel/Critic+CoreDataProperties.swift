//
//  Critic+CoreDataProperties.swift
//  
//
//  Created by Dmitry Zawadsky on 24.01.2018.
//
//

import Foundation
import CoreData


extension Critic {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Critic> {
        let fetchRequest = NSFetchRequest<Critic>(entityName: "Critic")
        let sort1 = NSSortDescriptor(key: #keyPath(Critic.index), ascending: true)
        fetchRequest.sortDescriptors = [sort1]
        fetchRequest.fetchLimit = Int.max
        return fetchRequest
    }

    @NSManaged var index: NSNumber?
    @NSManaged public var displayName: String?
    @NSManaged public var sortName: String?
    @NSManaged public var status: String?
    @NSManaged public var bio: String?
    @NSManaged public var seoName: String?
    @NSManaged public var multimedia: MultimediaCritic?

}
