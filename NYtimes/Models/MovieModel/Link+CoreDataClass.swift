//
//  Link+CoreDataClass.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//
//

import Foundation
import CoreData


public class Link: NSManagedObject, Codable {
    static let entityName = "Link"
    
    enum CodingKeys: String, CodingKey {
        case type
        case url
        case suggestedLinkText = "suggested_link_text"
    }
    
    public convenience init() {
        let context = Connector.viewContext
        guard let link = NSEntityDescription.entity(forEntityName: Link.entityName, in: context) else { fatalError() }
        
        self.init(entity: link, insertInto: context)
        
        self.type = ""
        self.url = ""
        self.suggestedLinkText = ""
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let context = Connector.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: "Link", in: context) else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.type = try container.decodeIfPresent(String.self, forKey: .type) ?? ""
        self.url = try container.decodeIfPresent(String.self, forKey: .url) ?? ""
        self.suggestedLinkText = try container.decodeIfPresent(String.self, forKey: .suggestedLinkText) ?? ""
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.type, forKey: .type)
        try container.encode(self.url, forKey: .url)
        try container.encode(self.suggestedLinkText, forKey: .suggestedLinkText)
    }
}
