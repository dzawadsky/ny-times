//
//  Movie+CoreDataClass.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//
//

import Foundation
import CoreData

public class Movie: NSManagedObject, Codable {
    
    static let movieCacheName = "movieCache"
    
    enum CodingKeys: String, CodingKey {
        case title = "display_title"
        case rating = "mpaa_rating"
        case criticsPick = "critics_pick"
        case byline
        case headline
        case summaryShort = "summary_short"
        case publicationDate = "publication_date"
        case openingDate = "opening_date"
        case dateUpdated = "date_updated"
        case link
        case multimedia
    }

    required convenience public init(from decoder: Decoder) throws {
        let context = Connector.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: "Movie", in: context) else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let dateFormatter = DateFormatter()
        
        self.index = NSNumber(value: MoviesHolder.nextIndex());
        self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        self.rating = try container.decodeIfPresent(String.self, forKey: .rating) ?? ""
        self.criticsPick = NSNumber(value: (try container.decodeIfPresent(Int64.self, forKey: .criticsPick) ?? 0))
        self.byline = try container.decodeIfPresent(String.self, forKey: .byline) ?? ""
        self.headline = try container.decodeIfPresent(String.self, forKey: .headline) ?? ""
        self.summaryShort = (try container.decodeIfPresent(String.self, forKey: .summaryShort) ?? "").htmlDecoded
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if let publicationDate = (try container.decodeIfPresent(String.self, forKey: .publicationDate)),
            let realDate = dateFormatter.date(from: publicationDate) {
            self.publicationDate = realDate
        } else {
            self.publicationDate =  Date.distantPast
        }
        if let openingDate = (try container.decodeIfPresent(String.self, forKey: .openingDate)),
            let realDate = dateFormatter.date(from: openingDate) {
            self.openingDate = realDate
        } else {
            self.openingDate =  Date.distantPast
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let dateUpdated = (try container.decodeIfPresent(String.self, forKey: .dateUpdated)),
            let realDate = dateFormatter.date(from: dateUpdated) {
            self.dateUpdated = realDate
        } else {
            self.dateUpdated =  Date.distantPast
        }
        
        self.link = try container.decodeIfPresent(Link.self, forKey: .link) ?? Link()
        self.multimedia = try container.decodeIfPresent(Multimedia.self, forKey: .multimedia) ?? Multimedia()
        
        Connector.appDelegate.saveContext()
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.title, forKey: .title)
        try container.encode(self.rating, forKey: .rating)
        try container.encode(self.criticsPick?.int64Value, forKey: .criticsPick)
        try container.encode(self.byline, forKey: .byline)
        try container.encode(self.headline, forKey: .headline)
        try container.encode(self.summaryShort, forKey: .summaryShort)
        try container.encode(self.publicationDate, forKey: .publicationDate)
        try container.encode(self.openingDate, forKey: .openingDate)
        try container.encode(self.dateUpdated, forKey: .dateUpdated)
        try container.encode(self.link, forKey: .link)
        try container.encode(self.multimedia, forKey: .multimedia)
    }
}
