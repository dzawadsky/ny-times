//
//  Multimedia+CoreDataClass.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//
//

import Foundation
import CoreData


public class Multimedia: NSManagedObject, Codable {   
    static let entityName = "Multimedia"
    
    enum CodingKeys: String, CodingKey {
        case type
        case src
        case width
        case height
    }
    
    public convenience init() {
        let context = Connector.viewContext
        guard let multimedia = NSEntityDescription.entity(forEntityName: Multimedia.entityName, in: context) else { fatalError() }
        
        self.init(entity: multimedia, insertInto: context)
        
        self.type = ""
        self.src = ""
        self.width = NSNumber(value: -1)
        self.height = NSNumber(value: -1)
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let context = Connector.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: "Multimedia", in: context) else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.type = try container.decodeIfPresent(String.self, forKey: .type) ?? ""
        self.src = try container.decodeIfPresent(String.self, forKey: .src) ?? ""
        self.width = NSNumber(value: (try container.decodeIfPresent(Int64.self, forKey: .width) ?? 0))
        self.height = NSNumber(value: (try container.decodeIfPresent(Int64.self, forKey: .height) ?? 0))
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.type, forKey: .type)
        try container.encode(self.src, forKey: .src)
        try container.encode(self.width?.int64Value, forKey: .width)
        try container.encode(self.height?.int64Value, forKey: .height)
    }
}
