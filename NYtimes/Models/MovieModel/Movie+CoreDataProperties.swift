//
//  Movie+CoreDataProperties.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//
//

import Foundation
import CoreData


extension Movie {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Movie> {
        let fetchRequest = NSFetchRequest<Movie>(entityName: "Movie")
        let sort1 = NSSortDescriptor(key: #keyPath(Movie.index), ascending: true)
        fetchRequest.sortDescriptors = [sort1]
        fetchRequest.fetchLimit = Int.max
        return fetchRequest
    }

    @NSManaged var index: NSNumber?
    @NSManaged var title: String?
    @NSManaged var rating: String?
    @NSManaged var criticsPick: NSNumber?
    @NSManaged var byline: String?
    @NSManaged var headline: String?
    @NSManaged var summaryShort: String?
    @NSManaged var publicationDate: Date?
    @NSManaged var openingDate: Date?
    @NSManaged var dateUpdated: Date?
    @NSManaged var link: Link?
    @NSManaged var multimedia: Multimedia?
}
