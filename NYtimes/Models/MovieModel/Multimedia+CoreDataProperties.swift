//
//  Multimedia+CoreDataProperties.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//
//

import Foundation
import CoreData


extension Multimedia {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Multimedia> {
        return NSFetchRequest<Multimedia>(entityName: "Multimedia")
    }

    @NSManaged var type: String?
    @NSManaged var src: String?
    @NSManaged var width: NSNumber?
    @NSManaged var height: NSNumber?
}
