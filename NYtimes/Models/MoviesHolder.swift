//
//  MovieModel.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//

import UIKit
import CoreData

class MoviesHolder: NSObject, Codable {
    
    private static var numOfMovies = 0
    
    var status = ""
    var copyright = ""
    var hasMore = true
    var numResults = 0
    var results = [Movie]()
    
    var count: Int {
        get {
            return results.count
        }
    }
    
    static let sharedInstance = MoviesHolder()
    
    //Modes
    var searchMode = false
    
    var dateMode = false
    var dateFilterString = ""
    
    enum CodingKeys: String, CodingKey {
        case status
        case copyright
        case hasMore = "has_more"
        case numResults = "num_results"
        case results
    }
    
    required convenience public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.init()
        
        self.status = try container.decodeIfPresent(String.self, forKey: .status) ?? ""
        self.copyright = try container.decodeIfPresent(String.self, forKey: .copyright) ?? ""
        self.hasMore = try container.decodeIfPresent(Bool.self, forKey: .hasMore) ?? false
        self.numResults = try container.decodeIfPresent(Int.self, forKey: .numResults) ?? 0
        self.results = try container.decodeIfPresent([Movie].self, forKey: .results) ?? [Movie]()
        
        MoviesHolder.sharedInstance.status = self.status
        MoviesHolder.sharedInstance.copyright = self.copyright
        MoviesHolder.sharedInstance.hasMore = self.hasMore
        MoviesHolder.sharedInstance.numResults = self.numResults
        MoviesHolder.sharedInstance.results = self.results
        
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(MoviesHolder.sharedInstance.status, forKey: .status)
        try container.encode(MoviesHolder.sharedInstance.copyright, forKey: .copyright)
        try container.encode(MoviesHolder.sharedInstance.hasMore, forKey: .hasMore)
        try container.encode(MoviesHolder.sharedInstance.numResults, forKey: .numResults)
        try container.encode(MoviesHolder.sharedInstance.results, forKey: .results)
    }
    
    static private func resetActiveHolder() {
        sharedInstance.status = ""
        sharedInstance.copyright = ""
        sharedInstance.hasMore = true
        sharedInstance.numResults = 0
        sharedInstance.results.removeAll()
    }
    
    func parseResponse( responseData: Data, completionHandler: (MoviesHolder?) -> Void) {
        let decoder = JSONDecoder()
        do {
            let moviesResponse = try decoder.decode(MoviesHolder.self, from: responseData)
            completionHandler(moviesResponse)
            return
        } catch {
            print(error)
        }
        completionHandler(nil)
    }
    
    static func nextIndex() -> Int {
        numOfMovies = numOfMovies + 1
        return numOfMovies
    }
}

