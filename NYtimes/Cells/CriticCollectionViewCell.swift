//
//  CriticCollectionViewCell.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 24.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//

import UIKit

class CriticCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var criticPhotoImageView: UIImageView!
    @IBOutlet weak var criticNameLabel: UILabel!
    
    static func identifier() -> String {
        return "CriticCollectionViewCell"
    }
    
    
    
}
