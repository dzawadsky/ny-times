//
//  MovieTableViewCell.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//

import UIKit

protocol MovieTableViewCellDelegate: class {
    func shareButtonDidClicked()
}

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var criticNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    weak var delegate: MovieTableViewCellDelegate?
    
    static let identifier = "MovieTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        DispatchQueue.main.async() {
//            let shadowPath = UIBezierPath(rect: self.coverImageView.bounds).cgPath
//            self.coverImageView.layer.shadowColor = UIColor(white: 0.7, alpha: 1.0).cgColor
//            self.coverImageView.layer.shadowOffset = CGSize(width: 3, height: 3)
//            self.coverImageView.layer.shadowOpacity = 0.9
//            self.coverImageView.layer.masksToBounds = false
//            self.coverImageView.layer.shadowPath = shadowPath
//            self.coverImageView.layer.shadowRadius = 10
//            self.coverImageView.layer.cornerRadius = 10
//            self.coverImageView.layer.masksToBounds = true
//        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func shareButtonTouchUpInside(_ sender: UIButton) {
        self.delegate?.shareButtonDidClicked()
    }
}
