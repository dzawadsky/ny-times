//
//  ViewController.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 20.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    enum TabIndex : Int {
        case reviewsViewControllerTab = 0
        case criticsViewControllerTab = 1
    }
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    var currentViewController: UIViewController?
    lazy var reviewsViewController: UIViewController? = {
        let reviewsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ReviewsViewController")
        return reviewsViewController
    }()
    lazy var criticsViewController : UIViewController? = {
        let criticsViewController = self.storyboard?.instantiateViewController(withIdentifier: "CriticsViewController")
        
        return criticsViewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.switchViewControllerForIndex(index: 0)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func segmentedControlValueChanged(_ segmentedControl: UISegmentedControl) {
        self.switchViewControllerForIndex(index: segmentedControl.selectedSegmentIndex)
    }
    
    func switchViewControllerForIndex( index: Int) {
        if let selectedViewController = self.viewControllerForSelectedSegmentIndex(index) {
            self.addChildViewController(selectedViewController)
            selectedViewController.didMove(toParentViewController: self)
            selectedViewController.view.frame = self.contentView.bounds
            self.contentView.addSubview(selectedViewController.view)
            self.currentViewController = selectedViewController
        }
    }

    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var selectedViewController: UIViewController?
        switch index {
        case TabIndex.reviewsViewControllerTab.rawValue:
            selectedViewController = self.reviewsViewController
        case TabIndex.criticsViewControllerTab.rawValue:
            selectedViewController = self.criticsViewController
        default:
            return nil
        }
        
        return selectedViewController
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
