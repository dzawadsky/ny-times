//
//  CriticsViewController.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 24.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//

import UIKit
import CoreData
import AlamofireImage
import CRRefresh

class CriticsViewController: UIViewController {

    @IBOutlet weak var criticsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //self.cleanTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateCritics {
            do {
                try Connector.criticsFetchedResultsController.performFetch()
            } catch {
                print(error)
            }
            self.criticsCollectionView.reloadData()
        }
        

    }
    
    func setRefreshers() {
        self.criticsCollectionView.cr.addHeadRefresh(animator: FastAnimator()) { [weak self] in
            self?.updateCritics {
                self?.cleanTableView()
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    self?.criticsCollectionView.cr.endHeaderRefresh()
                })
            }
        }
//        self.criticsCollectionView.cr.addFootRefresh(animator: NormalFooterAnimator()) { [weak self] in
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
//                self?.upda(nextMovies: true, completionHandler: { (isMoreAvailable) in
//                    self?.criticsCollectionView.cr.endLoadingMore()
//
//                    if !isMoreAvailable {
//                        self?.criticsCollectionView.cr.noticeNoMoreData()
//                    }
//                })
//            })
//        }
    }

    func updateCritics( nameToSearch: String = "", completionHandler: @escaping () -> Void = {}) {
        MovieAPI.critics(succes: { (response) in
            Connector.criticsHolder.parseResponse(responseData: response, completionHandler: { (criticsHolder) in
                completionHandler()
            })
        }, failure: { (error) in
            print(error)
            completionHandler()
        })
    }
    
    func cleanTableView() {
        UIView.performWithoutAnimation {
            for movie in Connector.criticsFetchedResultsController.fetchedObjects! {
                Connector.viewContext.delete(movie as! NSManagedObject)
            }
        }
    }
    
    func configure( cell: CriticCollectionViewCell, indexPath: IndexPath) {
        if let critic = Connector.criticsFetchedResultsController.object(at: indexPath) as? Critic {
            if let displayName = critic.displayName {
                cell.criticNameLabel.text = displayName
            }
            if let multimedia = critic.multimedia,
                let source = multimedia.resource.src,
                let url = URL(string: source) {
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                    size: cell.criticPhotoImageView.frame.size,
                    radius: 10.0
                )
                cell.criticPhotoImageView.af_setImage(
                    withURL: url,
                    placeholderImage: #imageLiteral(resourceName: "placeholder"),
                    filter: filter
                )
            } else {
                cell.criticPhotoImageView.image = #imageLiteral(resourceName: "placeholder")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CriticsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Connector.criticsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CriticCollectionViewCell
            .identifier(), for: indexPath) as! CriticCollectionViewCell
        
        self.configure(cell: cell, indexPath: indexPath)
        
        return cell
    }
    
    
}

extension CriticsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 2 - 20, height: collectionView.frame.size.width / 2 - 20)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
}
