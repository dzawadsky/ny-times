//
//  ReviewsViewController.swift
//  NYtimes
//
//  Created by Dmitry Zawadsky on 24.01.2018.
//  Copyright © 2018 Dmitry Zawadsky. All rights reserved.
//

import UIKit
import CoreData
import AlamofireImage
import CRRefresh

class ReviewsViewController: UIViewController {
    
    static let segueID = "ReviewsViewControllerSegue"
    
    @IBOutlet weak var moviesTableView: UITableView!
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var moviesTableViewTopLayoutConstraint: NSLayoutConstraint! //8-120
    
    var updateWithoutAnimation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        Connector.moviesFetchedResultsController.delegate = self
        
        self.setRefreshers()
        
        do {
            try Connector.moviesFetchedResultsController.performFetch()
        } catch {
            print(error)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateMovies()
    }
    
    
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        //let dateString = DateFormatter.localizedString(from: sender.date, dateStyle: .short, timeStyle: .none)
        self.dateTextField.text = dateFormatter.string(from: sender.date)
    }
    
    func toggleDatePicker() {
        self.view.layoutIfNeeded()
        
        var destAlpha: CGFloat
        if self.datePicker.alpha == 0.0 {
            self.moviesTableViewTopLayoutConstraint.constant = 128
            destAlpha = 1.0
        } else {
            self.moviesTableViewTopLayoutConstraint.constant = 8
            destAlpha = 0.0
        }
        UIView.animate(withDuration: 0.2, animations: {
            self.datePicker.alpha = destAlpha
            self.view.layoutIfNeeded()
        }, completion: { (completion) in
            
        })
    }
    
    func setRefreshers() {
        self.moviesTableView.cr.addHeadRefresh(animator: FastAnimator()) { [weak self] in
            self?.updateMovies { _ in
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    self?.moviesTableView.cr.endHeaderRefresh()
                })
            }
        }
        self.moviesTableView.cr.addFootRefresh(animator: NormalFooterAnimator()) { [weak self] in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                self?.updateMovies(nextMovies: true, completionHandler: { (isMoreAvailable) in
                    self?.moviesTableView.cr.endLoadingMore()
                    
                    if !isMoreAvailable {
                        self?.moviesTableView.cr.noticeNoMoreData()
                    }
                })
            })
        }
    }
    
    func updateMovies( nextMovies: Bool = false, completionHandler: @escaping ((_ isMoreAvailable: Bool) -> Void) = {_ in }) {
        if Connector.moviesHolder.hasMore || nextMovies == false {
            if !nextMovies {
                self.moviesTableView.cr.resetNoMore()
                if Connector.moviesCount > 0 {
                    self.cleanTableView()
                }
            }
            
            MovieAPI.reviews( offset: !nextMovies ? 0 : Connector.moviesCount, stringToSearch: self.searchTextField.text!, dateStringToSearch: self.dateTextField.text!, succes: { (response) in
                Connector.moviesHolder.parseResponse(responseData: response, completionHandler: { (moviesHolder) in
                    completionHandler(Connector.moviesHolder.hasMore)
                })
            }, failure: { (error) in
                print(error)
                completionHandler(Connector.moviesHolder.hasMore)
            })
        } else {
            completionHandler(Connector.moviesHolder.hasMore)
        }
    }
    
    func cleanTableView() {
        self.updateWithoutAnimation = true
        for movie in Connector.moviesFetchedResultsController.fetchedObjects! {
            Connector.viewContext.delete(movie as! NSManagedObject)
        }
        self.updateWithoutAnimation = false
    }
    
    func configure( cell: MovieTableViewCell, for indexPath: IndexPath) {
        if let movie = Connector.moviesFetchedResultsController.object(at: indexPath) as? Movie {
            cell.titleLabel.text = movie.title
            cell.descriptionLabel.text = movie.summaryShort
            cell.criticNameLabel.text = movie.byline
            
            if let dateUpdated = movie.dateUpdated {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
                cell.dateLabel.text = dateFormatter.string(from: dateUpdated)
            } else {
                cell.dateLabel.text = "N/A"
            }
            
            if let multimedia = movie.multimedia,
                let source = multimedia.src,
                let url = URL(string: source) {
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                    size: cell.coverImageView.frame.size,
                    radius: 10.0
                )
                cell.coverImageView.af_setImage(
                    withURL: url,
                    placeholderImage: #imageLiteral(resourceName: "placeholder"),
                    filter: filter
                )
            } else {
                cell.coverImageView.image = #imageLiteral(resourceName: "placeholder")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: UITableViewDelegate
extension ReviewsViewController: UITableViewDelegate, UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if self.datePicker.alpha != 0.0 {
            self.toggleDatePicker()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (tableView.frame.size.height / 3 + 10 > 240) ? tableView.frame.size.height / 3 + 10 : 240
    }
}

//MARK: UITableViewDataSource
extension ReviewsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Connector.moviesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.identifier) as! MovieTableViewCell
        
        self.configure(cell: cell, for: indexPath)
        
        return cell
    }
}

//MARK: NSFetchedResultsControllerDelegate
extension ReviewsViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        moviesTableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                moviesTableView.insertRows(at: [indexPath], with: self.updateWithoutAnimation ? .none : .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                moviesTableView.deleteRows(at: [indexPath], with: self.updateWithoutAnimation ? .none : .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath, let cell = moviesTableView.cellForRow(at: indexPath) {
                self.configure(cell: cell as! MovieTableViewCell, for: indexPath)
            }
            break;
        case .move:
            if let indexPath = indexPath {
                moviesTableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if let newIndexPath = newIndexPath {
                moviesTableView.insertRows(at: [newIndexPath], with: .fade)
            }
            break;
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        moviesTableView.endUpdates()
    }
}

extension ReviewsViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.dateTextField {
            self.toggleDatePicker()
            return true
        } else {
            if self.datePicker.alpha != 0 {
                self.toggleDatePicker()
            }
            
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if let textSearch = self.searchTextField.text,
            let textDate = self.dateTextField.text {
            
            Connector.searchMode = !textSearch.isEmpty
            Connector.dateMode = !textDate.isEmpty
            
            if textField == self.dateTextField {
                self.toggleDatePicker()
            }
            
            self.updateMovies()
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        textField.text = ""
        
        Connector.searchMode = !self.searchTextField.text!.isEmpty
        Connector.dateMode = !self.dateTextField.text!.isEmpty
        
        if textField == self.dateTextField {
            self.toggleDatePicker()
        }
        
        self.updateMovies()
        return false
    }
}

